/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

		
			function userDetails(){
				let fullName = prompt("What is your name?");
				let age = prompt("How old are you?");
				let address = prompt("Where do you live?");
				
				console.log('Hello, ' + fullName);
				console.log('You are ' + age + ' years old.');
				console.log('You live in ' + address);
				
			};

			userDetails();

			function thankYouAlert(){
				alert('Thank you for your input!');
			};

				thankYouAlert();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

			function favoriteArtists() {
			console.log('1. Justin Bieber');
			console.log('2. LANY');
			console.log('3. Taylor Swift');
			console.log('4. Ariana Grande');
			console.log('5. Dua Lipa');
			};

			favoriteArtists();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:


			function favoriteMovies(){				
				console.log('1. Catch Me If You Can');
				console.log('Rotten Tomatoes Rating: ' +96+'%');
				console.log('2. Inception');
				console.log('Rotten Tomatoes Rating: ' +87+'%');
				console.log('3. Hacksaw Ridge');
				console.log('Rotten Tomatoes Rating: ' +84+'%');
				console.log('4. Ocean\'s Eleven');
				console.log('Rotten Tomatoes Rating: ' +83+'%');
				console.log('5. The Wolf of Wall Street');
				console.log('Rotten Tomatoes Rating: ' +80+'%');

			};

			favoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

		function addFriends(){
			alert("Hi! Please add the names of your friends.");
		};

		addFriends();

		let printFriends = function printUsers(){
			let friend1 = prompt("Enter your first friend's name:"); 
			let friend2 = prompt("Enter your second friend's name:"); 
			let friend3 = prompt("Enter your third friend's name:");

			console.log("You are friends with:")
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
		};

		printFriends();